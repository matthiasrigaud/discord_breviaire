import discord
from discord.ext import commands

import time

# from aelfRequest import *
from write_logs import *
from jsonfiles2datas import *
import restrictions

# =========== CONSTANTES =====================

with open('./key.json') as f:
    _datas = json.load(f)
    _token = _datas['token']
    _datas=None
#endwith

_logFile = "/tmp/discord_breviaire.log"
_command_prefix="?"
# description du serveur
_description = "Prières catholiques du jour provenant du site de l'AELF (aelf.org) plus quelques ajouts."
# ============================================

# https://discord.com/developers/applications/983108100053696563/information
# https://dylanbonjean.wordpress.com/2018/01/05/bot-discord/


bot = commands.Bot(command_prefix=_command_prefix, description=_description)

try:
    @bot.event
    async def on_ready():
        t = str(time.ctime())
        print('Logged in as')
        print(bot.user.name)
        print(bot.user.id)
        print(t)
        print('------')

        write_log(
            user=bot.user,
            content="Bot \'{}\' started".format(bot.user.name),
            theFile=_logFile
        )
except Exception as e:
    write_log(
        user=bot.user,
        content="Bot \'{}\' **failed to start : {}**".format(bot.user.name, e),
        theFile=_logFile
    )
#endTry




@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.CommandNotFound):
        await ctx.send(F""":warning: La commande `{ctx.message.content}` n'existe pas :
- Vérifier l'orthographe
- Utiliser `{_command_prefix}sommaire`  ou  `{_command_prefix}help` pour voir la liste des commandes disponible
- Si vous pensez que cette commande devrait être implémentée, merci de lancer la commande  `{_command_prefix}request {ctx.message.content} (arguments s'il y en a) : précisions`""")
        write_log(
            user=ctx.author,
            content="Command not found : \'{}\'".format(ctx.message.content),
            theFile=_logFile
        )
        print(error)
    else:
        print(ctx.command, ctx.message)
        write_log(
            user="bot.event",
            content="Error raised in \"on_command_err()\"".format(error),
            theFile=_logFile
        )
        raise error
    #endIf
#endDef


# ================================================================== #
# ======================= FUNCTIONS ================================ #
# ================================================================== #
def priere_courte(contenu):
    retour=False
    for elt in ["court", "rapide"]:
        retour=retour or elt in contenu
    #endFor
    return retour
#endDef


async def main_bot(ctx, office=None, priere=None, **kwargs):
    allowed, msg = restrictions.allowUsage(ctx, logFile=_logFile)

    if not allowed:
        await ctx.send(":warning: {0}".format(msg))
        return 1
    #endIf

    if priere is not None:
        content = getContenuSupplementaire(priere, **kwargs)
    elif office is not None:
        content = getContentOffice(office,**kwargs)
    else:
        return 30
    #endIf

    for es in content:
        await ctx.send(".\n"+es+"\n.")
    #endFor

    await un_petit_retour(ctx)

    write_log(
        user=ctx.author,
        content="call '{0}' in channel '{1}' of the instance '{2}'".format(ctx.message.content, str(ctx.message.channel), str(ctx.message.guild.name)),
        theFile=_logFile
    )
    return 0
#endDef



# ================================================================== #
# =================== BOT'S FUNCTIONS ============================== #
# ================================================================== #

# ********************** Divers ************************************ #
@bot.command()
async def breviaire_infos(ctx):
    """À propos de l'application"""
    await ctx.send(description)
    await ctx.send("""Code python et support disponible sur
https://framagit.org/1sixunhuit/discord_breviaire
Merci d'y ouvrir un ticket en cas de bug ou pour toute demande.
Le développeur décline toute responsabilité à l'usage de cette application,
bien qu'il ait fait de son mieux.""")
#endDef

@bot.command()
async def breviaire_bug(ctx):
    """Signaler un bug"""
    generateLogs(ctx, theFile=_logFile)
#endDef

@bot.command()
async def helloU(ctx):
    """Fonction de test"""

    allowed, msg = restrictions.allowUsage(ctx, restrict_on="user", authorizedUsers="pmaltey", logFile=_logFile)

    if not allowed:
        await ctx.send(":warning: {0}".format(msg))
        return 1
    #endIf

    # ctx.author.name
    # ctv.author.mention
    await ctx.send("Hello, @{}  --  {}   :::  {} !! {}".format(ctx.author, ctx.author.mention, ctx.message.guild.name , ctx.message.channel))
#endDef



@bot.command()
async def request(ctx):
    """Demander une implémentation"""
    write_log(
        user=ctx.author,
        content="REQUEST : Creat the command : \'{}\'".format(ctx.message.content.replace("?request","")),
        theFile=_logFile
        )

    maintainer_id = await bot.fetch_user(readJson("key.json")["maintainer"]["id"])
    
    await ctx.send(F"La demande a été enregistrée.\n(pour notification : {maintainer_id.mention})")
#endDef

@bot.command()
async def sommaire(ctx):
    content="""`.
* Offices
    > ?laudes
    > ?lectures
    > ?sixte
    > ?none
    > ?vepres
    > ?complies                : avec confiteor du missel
    > ?complies confiteor      : avec              missel
    > ?complies missel         : avec              missel
    > ?complies dominicain     : avec              dominicains
    > ?complies sans confiteor : sans confiteor

* Prières
    > ?angelus
    > ?angelus_chant           : Angélus chanté
    > ?regina_caeli
    > ?reginacaeli             : alias pour (?regina_caeli)
    > ?yaedia                  : Prière pour Yaedia (version longue)
    > ?yaedia courte           :                    (version courte)

* Autre :
    > ?evangile                : Évangile de la messe du jour
"""

    """
    * À venir :
    > ?messe            (To do)
    > ?psaume           (To do): Psaume de la messe du jour
    > ?salve_regina
    `
    """
    
    await ctx.send(content)
    

# ********************** Offices ********************************** #

@bot.command()
async def lectures(ctx):
    """Office des lectures"""
    await main_bot(ctx=ctx,office="lectures")
    return 0
#endDef


@bot.command()
async def laudes(ctx):
    """Office des laudes"""
    await main_bot(ctx=ctx,office="laudes")
    return 0
#endDef

@bot.command()
async def tierce(ctx):
    """Office tierce"""
    await main_bot(ctx=ctx,office="tierce")
    return 0
#endDef

@bot.command()
async def sexte(ctx):
    """Office sexte"""
    await main_bot(ctx=ctx,office="sexte")
    return 0
#endDef

@bot.command()
async def none(ctx):
    """Office none"""
    await main_bot(ctx=ctx,office="none")
    return 0
#endDef

@bot.command()
async def vepres(ctx):
    """Office des vêpres"""
    await main_bot(ctx=ctx,office="vepres")
    return 0
#endDef

@bot.command()
async def complies(ctx):
    """Office des complies ; 'complies dominicain' > confiteor dominicain"""

    if "classique" in str(ctx.message.content).lower() or "sans" in str(ctx.message.content).lower():
        confiteor=None
    elif "dominicain" in str(ctx.message.content).lower():
        confiteor="dominicain"
    elif "missel" in str(ctx.message.content).lower() or "confiteor" in str(ctx.message.content).lower():
        confiteor="missel"
    else:
        #confiteor=None
        confiteor="missel"
    #endIf

    await main_bot(ctx=ctx,office="complies",confiteor=confiteor)
    return 0
#endDef

# ............. Messe / Evangile ......................................
# @bot.command()
# async def messe(ctx):
#     print(TODO)
#     #content = getContentOffice("complies")
#     content=["",]
#     #for es in content:
#     #    if es:
#     await ctx.send("TODO")
#     #endIf
#     #endFor
# #endDef

@bot.command()
async def evangile(ctx):
    """Évangile de la messe du jour"""
    content = getContentMesse("evangile")

    for es in content:
        await ctx.send(".\n"+es+"\n.")
    #endFor

    await un_petit_retour(ctx)
#endDef


# ........... Pas dans AELF (./contenu_supplementaire/) ...............

@bot.command()
async def angelus(ctx):
    """Angélus"""

    if "chant" in ctx.message.content:

        await  angelus_chant(ctx)
        return 0

    else:

        await main_bot(ctx=ctx,priere="angelus")
        return 0

    #endIf

#endDef

@bot.command()
async def angelus_chant(ctx):
    """Angélus chanté"""
    await main_bot(ctx=ctx,priere="angelus_chant")
    return 0
#endDef

@bot.command()
async def regina_caeli(ctx):
    """Regina cæeli"""
    await main_bot(ctx=ctx,priere="regina_caeli")
    return 0
#endDef

@bot.command()
async def reginacaeli(ctx):
    """Regina cæeli (alias)"""

    #await ctx.message.add_reaction("\U0001F47C")
    # https://www.fileformat.info/info/unicode/char/search.htm?q=angel&preview=entity

    await regina_caeli(ctx)
#endDef

@bot.command()
async def salve_regina(ctx):
    """Salve regina"""
    await main_bot(ctx=ctx,priere="salve_regina")
    return 0
#endDef

@bot.command()
async def salveregina(ctx):
    """Salve regina (alias)"""
    
    #await ctx.message.add_reaction("\U0001F47C")
    # https://www.fileformat.info/info/unicode/char/search.htm?q=angel&preview=entity

    await salve_regina(ctx)
#endDef


@bot.command()
async def yaedia(ctx):
    """Prière pour Yaedia"""

    await main_bot(ctx=ctx,
                   priere="pour_yaedia",
                   light=False,
                   priere_rapide=priere_courte(ctx.message.content))
    return 0

#endDef
# ======================================================== #

async def un_petit_retour(ctx, *emojis: discord.Emoji):
    msg = await ctx.reply("Le bot est actuellement en cours de tests.\nSi la moindre chose ne va pas, merci de le signaler avec \U0001F44E.")
    for emoji in ('👍', '👎'):
        await msg.add_reaction(emoji)
    #endFor
#endDef


@bot.event
async def on_reaction_add(reaction, user):
    emoji = reaction.emoji

    # Si la reaction est celle d'un bot
    if user.bot:
        return

    if not reaction.message.author.bot :
        # si on reagit sur un humain
        return
    elif reaction.message.author.name != "Bréviaire":
        # si on reagit sur un autre bot
        return
    #endIf

    if(reaction.message.reference is None):
        # les reactions sur l'integralite des messages ne me concernent pas : seul les votes m'interessent.
        return
    #endIf

    log_reaction="Reaction {s} on the message posted at {t} by the bot from {uu} : {c}".format(
        t=str(reaction.message.created_at),
        s=str(reaction),
        uu=str(reaction.message.reference.resolved.author),
        c=str(reaction.message.reference.resolved.content)
    )

    write_log(user=user,
                      content=(log_reaction),
                      theFile=_logFile
                      )


    fixed_channel = bot.get_channel(reaction.message.reference.channel_id)

    maintainer_id = await bot.fetch_user(readJson("key.json")["maintainer"]["id"])

    if emoji == "\U0001F44E": #thumbs down :thumbsdown:
        await fixed_channel.send("Merci de faire un retour détaillé des choses à améliorer sur le bot à {} ;)".format(maintainer_id.mention))
    #endIf


#endIf

# Attention : doit rester a la fin
bot.run(_token)
