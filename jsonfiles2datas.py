import sys
import re
import requests, json

from aelfRequest import *
from manageStrings import *
from write_logs import *

def readJson(filename):
    with open(filename,'r') as f:
        datas=json.load(f,strict=False)
    #endWith

    return datas
#endDef

def contenuAdditionnel(name,convertHTML=True,include=True,light=True, **kwargs):
    # convertHTML : Convertir le HTML
    # include : a inclure dans un message existant, ou s'en servir independamment ?
    # ligth : structure legere (office=content), par comparaison a la structure complete d'AELF

    try:
        content = readJson('./content_additional/{}.json'.format(name))

        datas = jsondatas2message(
            office_name=name,
            office_keys=content["deroulement"],
            office=content if light else content[name],
            convertHTML=convertHTML,
            **kwargs
        )

    except Exception as e:
        print(e)
        exit(13)
    #endTry

    if include:
        if convertHTML:
            return "\n".join(datas)
        else:
            return "<br/>".join(datas)
        #endif
    else:
        return datas
    return -1

#endDef

def getContentMesse(content_name, **kwargs):
    office_keys = readJson('./content_office/{}.json'.format(content_name))["messes"]

    if content_name=="evangile":
        return evangile_request(office_name="messes", office_keys=office_keys, **kwargs)
    else:
        exit(2)
    #endIf
#endDef

def getContentOffice(office_name, **kwargs):

    office_keys=readJson('./content_office/{}.json'.format(office_name))["deroulement"]
    return jsondatas2message(office_name=office_name, office_keys=office_keys, **kwargs)
#endDef

def getContenuSupplementaire(name, **kwargs):
    return contenuAdditionnel(name, convertHTML=True,include=False, **kwargs)
#endDef


# ============================================

def jsondatas2message(office_name, office_keys,
                      office=None,
                      LENMAX=1999,
                      confiteor=None,
                      convertHTML=True,
                      priere_rapide=False,
                      ):

    # Confiteor : None, missel (defaut), dominicain
    # convertHTML : appliquer conversion formatage pour enlever le HTML

    # Init :
    content_office = ["",]  # OUT : tableau de moins de LENMAX caracteres
    content=""

    if office is None:
        office = aelf2json(office_name, office_keys)
    #endIf

    for each_key in office_keys:
        if(type(each_key)==str):
            continue
        #endIf
        if("key" not in each_key.keys()):
            continue
        #endIf
        try:
            key=each_key["key"]
        except Exception as e:
            print("========================")
            print(office_keys)
            print("!!!!!!!!!!!!!!!!!!!!!!!!")
            print(each_key)
            print("........................")
            print(e)
            exit(22)
        #endTry

        if "duree" in each_key.keys():
            if priere_rapide and each_key["duree"] not in ["court","tout","short","all"]:
                continue
            #endIf
        #endIf

        if each_key == {} or key is None:

            #if postNow:

            content_office += [es for es in splitContent(content) if es]

            content=""

            #endIf
            continue
        #endIF

        postNow=False
        if office[key] == [] or str(office[key]) == "None":
            continue
        #endif

        try:
            the_ref=None

            if each_key["type"] == 0 : # On a le contenu directement a la valeur

                the_title   = each_key["name"]
                the_content = office[key]

            elif each_key["type"]==1 : # Le contenu est dans "texte", on utilise le titre du json ds le deroulement

                the_title   = each_key["name"]
                the_content = office[key]["texte"]

            elif each_key["type"]==2 : # Le contenu est dans "texte", on utilise le titre qui est present dans le retour de la requete

                the_title   = office[key]["titre"]
                the_content = office[key]["texte"]

            elif each_key["type"]==3 : # Idem que 1 sauf qu'on utilise en plus la reference presente dans retour de la requete

                the_title   = each_key["name"]
                the_ref=office[key]["reference"]
                the_content = office[key]["texte"]

            elif each_key["type"]==4 : # Entre 1, 2 et 3 : le titre est present, mais dans une autre cle et le texte est directement dedans

                the_title   = office[ each_key["key_title"] ]
                the_content = office[key]

            else:
                the_title   = ""
                the_content = ""
                print(key)
                #exit(9)
            #endIf

            if the_content != "None":

                # =========== Ajouts intercales ============= #

                # Doxologie
                use_doxo=False
                for elt in ["psaume", "cantique", "adieu","defunt"]:
                    use_doxo = use_doxo or elt in each_key["key"]
                #endFor
                if use_doxo:
                    the_content+="<br/>"+contenuAdditionnel("doxologie_heures",convertHTML=False)
                #endIf

                # Confiteor
                if office_name == "complies" and key=="introduction" and confiteor is not None: #confiteor
                    if confiteor == "dominicain":
                        confiteor="confiteor_dominicain"
                    else:
                        confiteor="confiteor_missel"
                    #endIf

                    the_content+="<br/><br/>"
                    addition=contenuAdditionnel(confiteor,convertHTML=False)
                    the_content+=addition
                #endIf

                # Intentions de priere
                if office_name in ["vepres",] and key=="intercession":
                    the_content+="<br/><br/>+++++(intercession)+++++++"
                    addition=""
                    #addition=contenuAdditionnel(intention_priere,convertHTML=False)
                    the_content+=addition
                #endIf
                # ========================================== #

                content_new, postNow = post_or_continue(content, appendTitleText(the_title, the_ref, the_content,convertHTML=convertHTML))

            #endIf

            if postNow:

                content_office += [es for es in splitContent(content) if es]

                content = content_new
            else:
                content += content_new
            #endIf
        except Exception as e:
            print(office_keys)

            sys.stderr.write(str(office[key])+"\n")
            sys.stderr.write(str(each_key)+"\n")

            sys.stderr.write(str(e)+"\n")
            exit(12)
        #endTry
    #endFor

    try:

        content_office += [es for es in splitContent(content) if es]

    except Exception as e:
        sys.stderr.write(str(office[key])+"\n")
        sys.stderr.write(e)
        exit(11)
    #endTry

    del content_office[0] # car on a init la liste avec ""

    return content_office






def evangile_request(office_name, office_keys, LENMAX=1999):

    office = aelf2json("messes", office_keys)[0]["lectures"]

    try:
        for eachLecture in office:
            if eachLecture["type"] !="evangile":
                continue
            #endIf

            the_intro   = eachLecture["intro_lue"]
            the_ref     = eachLecture["ref"]
            the_content = eachLecture["contenu"]

            the_content = "<p>".join(the_content.split("<p>")[:-1])  #ex : replace("– Acclamons la Parole de Dieu.</p>","")

            if the_content != "None":
                content_new, postNow = post_or_continue("", appendTitleText(the_intro, the_ref, the_content))
            #endIf

    except Exception as e:
            print(e)
            exit(6)
            pass
    #endTry


    content_office = [es for es in splitContent(content_new) if es]

    return content_office


if __name__=="__main__":
    print(
        contenuAdditionnel("pour_yaedia",convertHTML=True,include=False,light=False)
        )
