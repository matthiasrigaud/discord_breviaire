import sys
import time
def write_log(theFile, content="", user=None):

    now = time.ctime()

    if user is None:
        new_content="{date:<25}: {c}\n".format(
            date=str(now),
            c=content, u=user
        )
    else:
        new_content="{date:<25} (@{u}): {c}\n".format(
            date=str(now),
            c=content, u=user
        )
    
    with open(theFile, "a") as f:

        f.write(new_content)
    #endWith
    
    sys.stdout.write("New log at {date:<28}\n".format(date=str(now)))
    return 0
#endDef

if __name__=="__main__":
    fname="/tmp/test.log"
    write_log(theFile=fname, content="Lancer le test")
    write_log(theFile=fname, content="Lancer le test", user="Me")
    with open(fname,"r") as f:
        content=f.readlines()
        print(content)
    #endwith
