# Bot bréviaire

## Description

Ce bot utilise l'API d'AELF (aelf.org).

## Versions

### Passées
* v0.1.0 : premiers tests
* v0.2.0 : séparation des fichiers sources entre bot, traitement de la donnee et traitement des chaines de caractere
* v0.3.0 : utilisation de fichiers json pour les deroulements
* v0.4.0 : ajout de l'Evangile du jour
* v0.4.1 : Correction mise en forme des offices (saut de ligne, nouveau message)
* v0.4.2 : Gras sur ponctuation psalmaudique + suppression des espaces en fin de ligne
* v0.5.0 : Ajout de l'Angelus et du Regina Caeli
* v0.5.1 : Debug : gestion des accents HTML + suppresion de l'acclamation apres l'Evangile + ajout alias pour Regina Caeli
* v0.5.2 : Legere refonte de la facon de concatener les messages + ajout alias reginacaeli + creation log + debug
* v0.5.3 : Correction bug : titre patristrique devient le titre du texte patristique

### En debug
* v0.6.0 : Ajout de la doxologie apres les psaumes/cantiques + confiteor dans les complies
* v0.6.1 : Implementation de securites pour restreindre l'usage du bot
* v0.6.2 : Debug (re-enlever l'acclamation a la fin de l'Ev.), changt appel confiteor complies, suppression lead/trail spaces, modification nom dossier
* v0.6.3 : Factorisation des appels au bot + système de votes lié aux logs pour signaler un soucis + ajout prière defunt (Yaedia)
* v0.6.4 : Ajout possibilité prière raccourcie (Yaedia), ajout logs, ajout Antienne Zacharie, amélioration formattage texte
* v0.6.5 : Ajout Salve Regina + maj template key.json

### En développement


### Future

* v0.7 : Monitorage propre
* v0.8 : Ajout de la messe
* v0.9 : Refonte de la fonction de split
* v1.0 : Release stable

## Installation
### Prérequis (logiciels)
* python3
* python3-venv (optionnel, pour l'utilisation d'un environnement virtuel dédié)

### Prérequis (libraires Python)
* discord.py[voice]
* requests


### Installation

    $ python3 -m venv discord
    $ source discord/bin/activate
    $ pip install discord.py[voice] requests
    OR
    $ pip install -r ./requirements_python.txt


### Utilisation
* Lancement du serveur, dans le dossier avec le programme Python :

    $ python3 breviaire_bot.py

* Dans Discord, appeler l'une des commandes suivantes

* Bot
    > ?help
    > ?breviaire_infos
    > ?breviaire_bug (dire ce qui ne va pas) : ATTENTION : pas certian que ca fonctionne
    > ?sommaire                : Afficher la liste des offices, prières etc
    > ?request                 : Demander qqc aux mainteneur du bot
    
* Offices
    > ?laudes
    > ?lectures
    > ?sixte
    > ?none
    > ?vepres
    > ?complies                : avec confiteor du missel
    > ?complies confiteor      : avec              missel
    > ?complies missel         : avec              missel
    > ?complies dominicain     : avec              dominicains
    > ?complies sans confiteor : sans confiteor

* Prières
    > ?angelus
    > ?angelus_chant           : Angélus chanté
    > ?regina_caeli
    > ?reginacaeli             : alias pour (?regina_caeli)
    > ?yaedia                  : Prière pour Yaedia (version longue)
    > ?yaedia courte           :                    (version courte)

* Autre :
    > ?evangile                : Évangile de la messe du jour

* À venir :
    > ?messe            (To do)
    > ?psaume           (To do): Psaume de la messe du jour
    > ?salve_regina

## To-do list
### Questions :
* Quand il n'y a pas d'antienne a un psaume, ca se passe comment ?


### Divers :
* Logo a refaire (droits)

### Code :
#### Usage :
* Incorporer la messe + le psaume du jour uniquement
* Appeler extrait d'un office
* Ajouter Salve Regina
* Supprimer sommaire "a venir"

#### Admin :
* Monitorage utilisation/erreur
#### Code :
* Rendre lisible fonction de split
* Vérifier tout le contenu avec le site AELF => vérifier que toutes les clés ont été utilisées
* Ne pas planter si une clé n'existe pas mais sortir un message d'erreur


## Documentation
* https://discordpy.readthedocs.io/en/stable/api.html