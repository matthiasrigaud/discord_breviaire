import os
from write_logs import *


def allowUsage(ctx,restrict_on=None, authorizedUsers=None, authorizedChannels=None,
               restrictionFile="./security/restrictions.json",
               logFile=None):

    # Priorite sur les arguments de la fonctions, pas sur le fichier
    useFile=os.path.exists(restrictionFile)

    if useFile:
        jsonRestrictions = readJson()
    #endIf

    if restrict_on is None and useFile:
        try:
            restrict_on=jsonRestrictions["restrict_on"]
        except:
            pass
        #endTry
    #endIf
    if authorizedUsers is None and useFile:
        try:
            authorizedUsers=jsonRestrictions["authorizedUsers"]
        except:
            pass
        #endTry
    #endIf
    if authorizedChannels is None and useFile:
        try:
            authorizedChannels=jsonRestrictions["authorizedChannels"]
        except:
            pass
        #endTry
    #endIf

    if restrict_on is None:
        return True, ""
    #endIf

    allow   = True
    message = ""

    username= str(ctx.author.name)
    userfullname = str(ctx.author)
    cmd=str(ctx.message.content)
    channel = str(ctx.message.channel)
    instance= str(ctx.message.guild.name)

    #print(str(username)+"  "+str(channel)+"  "+str(instance))

    # on author (! Keep this one in first place) :
    if ( "user" in restrict_on ) and ( authorizedUsers is not None) :

        if type(authorizedUsers)==str:
            authorizedUsers = [authorizedUsers,]
        #endIf

        if ( username in authorizedUsers ) or ( userfullname in authorizedUsers ) :
            allow = True
        else:
            allow = False
            message = "This command can only be called by : {}\n".format(authorizedUsers)
        #endIf

    #endIf


    # on channel
    if ( "channel" in restrict_on ) and (authorizedChannels is not None):
        if type(authorizedChannels)==str:
            authorizedChannels = [authorizedChannels,]
        #endIf

        if (channel in authorizedChannels):
            allow = allow
        else:
            allow = False
            message += "You can call this command only in one the following channels : {}\n".format(list(authorizedChannels))
        #endIf
    #endIf

    # Message d'erreur
    if logFile is not None:
        if allow:
            write_log(user=userfullname,
                      content=(
                          "User call \'{cmd}\' from the bot in the channel ".format(cmd=cmd)+
                          "{channel} of the instance {inst}".format(
                              channel=channel,
                              inst=instance)
                      ),
                      theFile=logFile
                      )
        else:
            write_log(user=userfullname,
                      content=(
                          "Try (unsuccessfully) to call \'{cmd}\' from the bot in the channel ".format(cmd=cmd)+
                          "{channel} of the instance {inst}".format(
                              channel=channel,
                              inst=instance)
                      ),
                      theFile=logFile
                      )
        #endIf
    #endIf

    return allow, message
