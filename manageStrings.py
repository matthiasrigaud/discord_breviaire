import re

LENMAX=1999
# Todo : fonction a degage
def post_or_continue(content_old, addition):
    return (addition, len(content_old)+len(addition)>LENMAX)
#endDef

# @brief : Nettoyage fonction
# Todo : renommer : html2discord
def formatMsg(content):

    # Enlever mots en majuscule
    content=" ".join([
        word.title() if word == word.upper()
        else word
        for word in content.split(" ")
    ])

    # Ajout gras la ou il n'y en a pas
    content=content.replace("(R/)","<b>(R/)</b> ").replace("R/","<b>R/</b> ")
    content=content.replace("V/","<b>V/</b> ")

    # Doublons (pre)
    content=content.replace("<span><span>","").replace("</span></span>","")

    # Simplifications
    content=content.replace("<span class=\"verse_number\">","<b>").replace("</span>"," </b>").replace("<span>","<b>")
    content=content.replace("<strong>","<b>").replace("</strong>","</b>")
    content=content.replace("<em>","<i>").replace("</em>","</i>")

    # # Doublons (post)
    for symb in ["b","u","i"]:
        for i in [0,1,2]:
            content="\n".join([re.sub(r'\<{0}\>(.*?)\<{0}\>'.format(symb), r'\1<{0}>'.format(symb), line)
                               for line in content.split("\n")])
            content="\n".join([re.sub(r'\<\/{0}\>(.*?)\<\/{0}\>'.format(symb), r'\1</{0}>'.format(symb), line)
                               for line in content.split("\n")])
        #endFor i  => s il y a des doublons/triplons dans les repetitions de balises
    #endFor

    # Remplacements
    content=content.replace("*"," <b>\*</b> ").replace("+"," <b>+</b> ").replace("\n","")

    content=content.replace("\n\n","<br /><br />")
    content=content.replace("<p>","").replace("</p>","\n")
    content=content.replace("<br>","\n").replace("<br />","\n").replace("<br/>","\n")

    content=content.replace("<u>","__").replace("</u>","__")
    content=content.replace("<i>","*").replace("</i>","*")
    content=content.replace("<b>","**").replace("</b>","**")

    # Accents
    # https://www.journaldunet.com/solutions/dsi/1195751-accents-caracteres-speciaux-html/
    content=content.replace("&agrave;","à").replace("&Agrave;","À")
    content=content.replace("&acirc;","â").replace("&Acirc;","Â")
    content=content.replace("&eacute;","é").replace("&Eacute;","É")
    content=content.replace("&egrave;","è").replace("&Egrave;","È")
    content=content.replace("&ecirc;","ê").replace("&Ecirc;","Ê")
    content=content.replace("&euml;","ë").replace("&Euml;","Ë")
    content=content.replace("&aelig;","æ").replace("&AEmig;","Æ")
    content=content.replace("&nbsp;","\xa0")
    content=content.replace("&rsquo;","'")
    content=content.replace("&gt;",">").replace("&lt;","<")
    content=content.replace("&star;","\*")


    content=content.replace("\n\n\n","\n\n").replace("\n\n\n\n","\n\n")
    content="\n".join([e.strip() for e in content.split("\n")]).strip()

    return content
#endDef


def appendTitleText(title=None,ref=None,content="",convertHTML=True):
    if content == "" or content is None:
        return ""
    #endIf
    out="\n"

    if ref is None:
        ref_formated = ""
    elif convertHTML:
        ref_formated = " (*{0}*)".format( formatMsg(ref) )
    else:
        ref_formated = " (<i>{0}</i>)".format( formatMsg(ref) )
    #endIf
    if title is None:
        title_formated = ""
    elif convertHTML :
        title_formated = "**{0}**".format(formatMsg(title))
    else:
        title_formated = "<b>{0}</b>".format(formatMsg(title))
    #endIf

    title_complet="{ttl}{ref}".format(ttl=title_formated, ref=ref_formated)

    newline = "\n" if convertHTML else "<br/>"

    if title is not None:
        out +="{newline}{lft}{ttl}{rgt}".format(
            lft="\*\>" if convertHTML else "&star;&gt;",
            rgt="\<\*" if convertHTML else "&lt;&star;",
            ttl=title_complet.center(45, " "),
            ref=ref_formated,
            newline=newline,
        )
    #endIf
    out+="{newline}{txt}{newline}".format(
        txt=formatMsg(content)  if convertHTML else content,
        newline=newline)


    return out
#endDef


# @brief Split
#             - default : sur les retours a la ligne
#             - si ligne trop longue : sur la ponctuation . ! ? : ;
#             - si ligne encore trop longue : sur la ponctuation ,
#             - si ligne encore trop longue : sur les espace
#             Et on s'arrete la : le mot "anticonstitutionnellement" faisant moins de 2000 caracteres

#ToDo : intentation, rendre lisible, optimiser
def splitContent(content):

        ltmp=[e+"\n" for e in content.split("\n")]

        i=0
        lcontent = ["",]
        for e in ltmp:
            if len(e) >= LENMAX:
                ls = re.findall(r'(?:\d[.\!\?\:\;]|[^.\!\?\:\;])*(?:[.\!\?\:\;]|$)', e)
                j=0
                le = ["",]
                for f in ls:
                    if not f:
                        continue
                    #endIf
                    k = None
                    if len(f) >= LENMAX:
                        lff = re.findall(r'(?:\d[,]|[^,])*(?:[,]|$)', f)
                        lf = ["",]
                        k = 0
                        l = None
                        for g in lff:
                            if len(g) >= LENMAX:
                                lgg=[h+" " for h in g.split(" ")]
                                l = 0
                                lg = ["",]
                                for h in lgg:
                                    if len(lg[l]) +len(h) >= LENMAX:
                                        l += 1
                                        lg += ["",]
                                    #endIf
                                    lg[l] += h
                                #endFor
                                lf += lg
                                k += l + 1

                            elif len(lf[j]) + len(g) >= LENMAX:
                                k  += 1
                                lf += ["",]
                            #endIf
                            if l is None:
                                lf[k] += g
                            #endIf
                        #endFor
                    elif len(le[j]) + len(f) >= LENMAX:
                        j  += 1
                        le += ["",]
                    #endIf

                    if k is None:
                        le[j] += f
                    else:
                        j += k+1
                        le +=lf
                    #endFor
                #endFor

                i+=j+1
                lcontent+=le

            else:
                if len(lcontent[i])+len(e) >= LENMAX:
                    i += 1
                    lcontent += ["",]
                #endIf
                lcontent[i] += e
            #endIf
        #endFor

        return list([e for e in lcontent if e not in ["\n", ""]])
    #endDef
