import requests, json
from datetime import date, time

def aelf2json(office_name, office_keys):

    # Initialisations

    today = date.today()
    time="{0:04d}-{1:02d}-{2:02d}".format(int(today.year),int(today.month),int(today.day))

    zone="france"

    requested_url="https://api.aelf.org/v1/{0}/{1}/{2}".format(office_name,time,zone)

    url = requests.get(requested_url)
    text = url.text

    datas_from_aelf = json.loads(text)

    office=datas_from_aelf[office_name]

    return office



